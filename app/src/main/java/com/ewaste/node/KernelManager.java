package com.ewaste.node;

import com.ewaste.kernel.IKernel;

import android.util.Log;
import android.util.Pair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import dalvik.system.DexClassLoader;


class KernelRunnable implements Runnable {
    private IKernel kernel;
    private byte[] input;
    volatile byte[] result;

    KernelRunnable(IKernel kernel, byte[] input) {
        this.kernel = kernel;
        this.input = input;
    }

    @Override
    public void run() {
        this.result = kernel.execute(input);
    }
}

public class KernelManager {

    private IKernel kernel;
    private File filesDir;

    public KernelManager(File filesDir, int coresCount) {
        this.filesDir = filesDir;
    }

    void loadKernel(String className, byte[] dex) throws IOException, ClassNotFoundException,
            InstantiationException, IllegalAccessException {
        File kernelsDir = new File(filesDir, "kernels");
        kernelsDir.mkdir();
        File optimizedDir = new File(kernelsDir, "optimized");
        optimizedDir.mkdir();
        File file = new File(kernelsDir, className + ".dex");
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(dex);
        outputStream.close();
        DexClassLoader dLoader = new DexClassLoader(file.getPath(), optimizedDir.getAbsolutePath(),
                null, this.getClass().getClassLoader());
        Class loadedClass = dLoader.loadClass(className);
        kernel = (IKernel) loadedClass.newInstance();
    }

    List<byte[]> execute(List<byte[]> inputs) {
        List<Pair<KernelRunnable, Thread>> threads = new ArrayList<Pair<KernelRunnable, Thread>>();
        for(byte[] input : inputs) {
            KernelRunnable r = new KernelRunnable(kernel, input);
            Thread t = new Thread(r);
            t.start();
            threads.add(new Pair<KernelRunnable, Thread>(r, t));
        }
        List<byte[]> results = new ArrayList<byte[]>();
        try {
            for(Pair<KernelRunnable, Thread> p : threads) {
                p.second.join();
                results.add(p.first.result);
            }
        } catch (Exception e) {
            Log.e("error", "Kernel execution error: " + e.getMessage());
        }
        return results;
    }
}
