package com.ewaste.node;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class HandshakeCaps {

    private long memory;
    private int cores;

    public long getMemory() { return memory; }
    public int coreCount() { return cores; }

    HandshakeCaps() {
        memory = Runtime.getRuntime().totalMemory();
        cores = Runtime.getRuntime().availableProcessors();
    }

    JSONObject asJSON() {
        JSONObject message = new JSONObject();
        try {
            message.put("message", "caps");
            JSONObject caps = new JSONObject();
            caps.put("memory", memory);
            caps.put("cores", cores);
            message.put("caps", caps);
        } catch(JSONException e) {
            Log.e("error", "error parsing caps" + e.getMessage());
        }
        return message;
    }

}
