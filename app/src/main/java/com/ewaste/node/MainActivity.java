package com.ewaste.node;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity implements Button.OnClickListener, IConnectionCallback {

    private Button joinButton = null;
    private EditText host = null;
    private TCPSocketConnection connection = null;
    private KernelManager kernelManager;
    private HandshakeCaps caps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        joinButton = (Button)findViewById(R.id.button_join);
        if(joinButton != null)
            joinButton.setOnClickListener(this);

        host = (EditText)findViewById(R.id.hostName);
        caps = new HandshakeCaps();
        kernelManager = new KernelManager(getBaseContext().getFilesDir(), caps.coreCount());
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.button_join && host != null) {
            view.setEnabled(false);
            connection = TCPSocketConnection.getInstance(host.getText().toString(), kernelManager, caps);
            connection.connect(this);
        }
    }

    public void loggerLog(final String string) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView logger = (TextView)findViewById(R.id.logger);
                logger.append(string);
            }
        });
    }

    @Override
    public void onConnected() {
        loggerLog("Connected!\nSending handshake caps\n");
    }

    @Override
    public void onFailed(final String message) {
        loggerLog("ERROR! "+message+"\n");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                joinButton.setEnabled(true);
            }
        });
    }

    @Override
    public void onLog(String message) {
        loggerLog("[i] "+ message + "\n");
    }

    @Override
    public void onShutdown() {
        loggerLog("Connection shut down.\n");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                joinButton.setEnabled(true);
            }
        });
    }


}
