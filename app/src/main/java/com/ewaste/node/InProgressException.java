package com.ewaste.node;

public class InProgressException extends Exception {
    public InProgressException(String msg) {
        super(msg);
    }
}
