package com.ewaste.node;

public interface IConnectionCallback {

    void onConnected();
    void onFailed(String message);
    void onLog(String message);
    void onShutdown();

}
