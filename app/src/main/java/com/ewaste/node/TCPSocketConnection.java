package com.ewaste.node;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import net.iharder.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class TCPSocketConnection implements Runnable {

    private static TCPSocketConnection connectionInstance;

    public static TCPSocketConnection getInstance(String hostname, KernelManager kernelManager, HandshakeCaps caps) {
        if(connectionInstance == null) {
            connectionInstance = new TCPSocketConnection(hostname, kernelManager, caps);
            return connectionInstance;
        } else {
            if(connectionInstance.connectedTo(hostname)) return connectionInstance;
            else {
                new Thread(new DisconnectRunnable(connectionInstance, connectionInstance.getCallback())).start();
                connectionInstance = new TCPSocketConnection(hostname, kernelManager, caps);
                return connectionInstance;
            }
        }
    }

    private Thread selfThread = null;
    private KernelManager kernelManager;
    private HandshakeCaps caps;

    private TCPSocketConnection(String hostname, KernelManager kernelManager, HandshakeCaps caps) {
        this.hostname = hostname;
        this.kernelManager = kernelManager;
        this.caps = caps;
    }

    private void setHostname(String hostname) {
        this.hostname = hostname;
    }

    private IConnectionCallback callback = null;
    private Socket socket = null;
    private String hostname;

    public IConnectionCallback getCallback() { return callback; }

    // Interface for connection from the UI
    void connect(IConnectionCallback callback) {
        this.callback = callback;
        if(socket != null && socket.isConnected()) return;
        try {
            asyncConnect(hostname);
        } catch (InProgressException e) {
            if(callback != null)
                callback.onFailed(e.getMessage());
        }
    }

    // Are we connected to `hostname`
    boolean connectedTo(String hostname) {
        return (socket != null && this.hostname.equals(hostname) && socket.isConnected());
    }

    private void asyncConnect(String hostname) throws InProgressException {
        if(selfThread != null && selfThread.getState() != Thread.State.TERMINATED)
            throw new InProgressException("Connection in progress");
        else {
            selfThread = new Thread(this);
            selfThread.start();
        }
    }

    private InputStream inputStream;
    private OutputStream outputStream;
    private boolean accept = true;

    void stop() {
        accept = false;
        try {
            inputStream.close();
            outputStream.close();
            selfThread.join(5000);
            selfThread.interrupt();
        } catch (Exception e) {
            callback.onFailed( e.getMessage() );
        }
    }

    private void shutdown() {
        Thread shutdownThread = new Thread(new DisconnectRunnable(this, callback));
        shutdownThread.start();
    }

    private void onFailed(String error) {
        callback.onFailed(error);
        shutdown();
    }

    private void onShutdown() {
        callback.onShutdown();
        shutdown();
    }

    private void handshake() {
        String baseError = "Handshake error";
        try {
            JSONObject greetMsg = receive(3);
            if (!greetMsg.getString("command").equals("greet")) {
                onFailed(baseError);
            }
            callback.onLog(String.format("Server assigned ID: %X", greetMsg.getLong("id")));

            JSONObject capsJSON = caps.asJSON();
            send(capsJSON);
        } catch (Exception e) {
            onFailed(baseError + ": " + e.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            socket = new Socket(hostname, 19955);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            callback.onConnected();
            handshake();
            while(accept) { //main loop for receiving commands
                JSONObject msg = receive(3);
                String cmd = msg.getString("command");
                if (cmd.equals("kernel")) {
                    receiveKernel(msg);
                } else if(cmd.equals("data")) {
                    receiveData(msg);
                } else if (cmd.equals("bye")) {
                    receiveGoodbye();
                    onShutdown();
                } else {
                    onFailed("Socket connection: wrong command.");
                }
            }
        } catch (Exception e) {
            if(accept)
                onFailed("Socket connection error: " + e.getMessage());
        }

    }

    private void receiveKernel(JSONObject msg) {
        try {
            String kernelClassName = msg.get("kernelClassName").toString();
            byte[] kernelDex = Base64.decode(msg.getString("kernelDex"));
            kernelManager.loadKernel(kernelClassName, kernelDex);
            send(new JSONObject("{\"message\":\"ack\"}"));
            callback.onLog("Received new kernel " + kernelClassName);
        } catch(Exception e) {
            onFailed("Kernel receive failed: " + e.getMessage());
        }
    }

    private void receiveData(JSONObject msg) {
        try {
            JSONArray arr = msg.getJSONArray("data");
            JSONArray arrOut = new JSONArray();
            callback.onLog("Received data.");
            List<byte[]> inputs = new ArrayList<byte[]>();
            for(int i=0;i<arr.length();i++) {
                inputs.add(Base64.decode(arr.getString(i)));
            }
            List<byte[]> results = kernelManager.execute(inputs);
            for(byte[] result : results) {
                arrOut.put(Base64.encodeBytes(result));
            }
            JSONObject resultJSON = new JSONObject("{\"message\":\"result\"," +
                    "\"result\":" + arrOut.toString() + "}");
            send(resultJSON);
            callback.onLog("Sent result.");
        } catch(Exception e) {
            onFailed("Data receive failed: " + e.getMessage());
        }
    }

    private void receiveGoodbye() {
        try {
            send(new JSONObject("{\"message\":\"ack\"}"));
            callback.onLog("Waved goodbye.");
        } catch(Exception e) {
            onFailed("Goodbye failed." + e.getMessage());
        }
    }

    private JSONObject receive(int waitLimit) throws TimeoutException, InterruptedException,
            IOException, JSONException {
        int retry = 0;
        while(retry < waitLimit && inputStream.available() <= 0) {
            Thread.sleep(1000);
            retry++;
        }
        if(retry >= waitLimit) {
            throw new TimeoutException();
        } else {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            return new JSONObject(new String(buffer));
        }
    }

    private void send(JSONObject obj) throws IOException{
        outputStream.write(obj.toString().getBytes());
        outputStream.flush();
    }
}

class DisconnectRunnable implements Runnable {
    private TCPSocketConnection target;
    private IConnectionCallback callback;
    public DisconnectRunnable(TCPSocketConnection conn, IConnectionCallback callback) {
        this.target = conn;
        this.callback = callback;
    }

    @Override
    public void run() {
        try {
            target.stop();
        } catch (Exception e) {
            if(callback != null)
                callback.onFailed("Disconnection thread -> " + e.getMessage());
        }
    }
}